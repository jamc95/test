<?php

namespace AppBundle\Controller;

use AppBundle\Service\xmlRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class xmlController extends Controller
{
    /**
     * @Route("/xml", name="xml_info")
     * methods={"POST"}
     */
    public function xmlAction(Request $request)
    {
        $r = new xmlRequest();

        $url = "https://devapi.multisafepay.com/ewx/";
        $data = '<?xml version="1.0" encoding="UTF-8"?>
<status ua="custom-1.1"> 
    <merchant> 
      <account>11018887</account> 
       <site_id>393</site_id> 
       <site_secure_code>970443</site_secure_code> 
   </merchant> 
   <transaction> 
       <id>jose_antonio_2</id>
   </transaction> 
</status>
';

       $rawContent =  $r->xmlGetData($url, $data);


        $response = new Response($rawContent);
        $response->headers->set('Content-Type', 'xml');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
}
