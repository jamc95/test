<?php

namespace AppBundle\Service;

class xmlRequest{
    public function xmlGetData($url, $input_xml) {
        $ch = curl_init();


        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/xml'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $input_xml);



        $data = curl_exec($ch);


        curl_close($ch);

        return $data;
    }
}